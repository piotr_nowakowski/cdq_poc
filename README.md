

## Sample single run results

format: [job_global_id] [status] [message]

    python poc.py -s 

    [1_20200611111152628893] [INFO] ***********************job 1_20200611111152628893 started***********************
    [1_20200611111152628893] [INFO] progress 0 status INITIATED
    [1_20200611111152628893] [INFO] checking vars
    [1_20200611111152628893] [INFO] progress 10 status RUNNING
    [1_20200611111152628893] [INFO] fetching data from repo
    [1_20200611111152628893] [INFO] extracting data using cli criteria name=company abc and cli flags skip=1;limit=10 using db mongo_prod
    [1_20200611111152628893] [INFO] saving to .\BusinessPartnersUpdatesReport_1_20200611111152628893.xlsx
    [1_20200611111152628893] [INFO] saving bp_updates sheet
    [1_20200611111152628893] [INFO] saving bp_data sheet
    [1_20200611111152628893] [INFO] formatting report .\BusinessPartnersUpdatesReport_1_20200611111152628893.xlsx
    [1_20200611111152628893] [INFO] complex excel formatting
    [1_20200611111152628893] [INFO] progress 50 status RUNNING
    [1_20200611111152628893] [INFO] upload status to 'https://prod...agement-api/1' -> 50,RUNNING
    [1_20200611111152628893] [INFO] waiting...
    [1_20200611111152628893] [INFO] ok
    [1_20200611111152628893] [INFO] shipping report
    [1_20200611111152628893] [INFO] progress 90 status RUNNING
    [1_20200611111152628893] [INFO] upload status to 'https://prod...agement-api/1' -> 90,RUNNING
    [1_20200611111152628893] [INFO] waiting...
    [1_20200611111152628893] [INFO] ok
    [1_20200611111152628893] [INFO] upload report to 'https://prod...reporting-api' .\BusinessPartnersUpdatesReport_1_20200611111152628893.xlsx
    [1_20200611111152628893] [INFO] waiting...
    [1_20200611111152628893] [INFO] ok
    [1_20200611111152628893] [INFO] performing cleanup..
    [1_20200611111152628893] [INFO] progress 100 status FINISHED
    [1_20200611111152628893] [INFO] upload status to 'https://prod...agement-api/1' -> 100,FINISHED
    [1_20200611111152628893] [INFO] waiting...
    [1_20200611111152628893] [INFO] ok
    [1_20200611111152628893] [INFO] ************************job 1_20200611111152628893 ended************************
    report {'bp_updates': 0    company abc
    Name: 0, dtype: object, 'bp_data':              0   1   2    3
    0  company abc  15  it  2.5}



## Sample batch run results
    
    python poc.py -b

    [1_20200611111024531572] [INFO] ***********************job 1_20200611111024531572 started***********************
    [1_20200611111024531572] [INFO] progress 0 status INITIATED
    [1_20200611111024531572] [INFO] checking vars
    [1_20200611111024531572] [INFO] progress 10 status RUNNING
    [1_20200611111024531572] [INFO] fetching data from repo
    [1_20200611111024531572] [INFO] extracting data using cli criteria name=company abc and cli flags skip=1;limit=10 using db mongo_prod
    [1_20200611111024531572] [INFO] saving to .\BusinessPartnersUpdatesReport_1_20200611111024531572.xlsx
    [1_20200611111024531572] [INFO] saving bp_updates sheet
    [1_20200611111024531572] [INFO] saving bp_data sheet
    [1_20200611111024531572] [INFO] formatting report .\BusinessPartnersUpdatesReport_1_20200611111024531572.xlsx
    [1_20200611111024531572] [INFO] complex excel formatting
    [1_20200611111024531572] [INFO] progress 50 status RUNNING
    [1_20200611111024531572] [INFO] upload status to 'https://prod...agement-api/1' -> 50,RUNNING
    [1_20200611111024531572] [INFO] waiting...
    [1_20200611111024531572] [INFO] ok
    [1_20200611111024531572] [INFO] shipping report
    [1_20200611111024531572] [INFO] progress 90 status RUNNING
    [1_20200611111024531572] [INFO] upload status to 'https://prod...agement-api/1' -> 90,RUNNING
    [1_20200611111024531572] [INFO] waiting...
    [1_20200611111024531572] [INFO] ok
    [1_20200611111024531572] [INFO] upload report to 'https://prod...reporting-api' .\BusinessPartnersUpdatesReport_1_20200611111024531572.xlsx
    [1_20200611111024531572] [INFO] waiting...
    [1_20200611111024531572] [INFO] ok
    [1_20200611111024531572] [INFO] performing cleanup..
    [1_20200611111024531572] [INFO] progress 100 status FINISHED
    [1_20200611111024531572] [INFO] upload status to 'https://prod...agement-api/1' -> 100,FINISHED
    [1_20200611111024531572] [INFO] waiting...
    [1_20200611111024531572] [INFO] ok
    [1_20200611111024531572] [INFO] ************************job 1_20200611111024531572 ended************************
    report {'bp_updates': 0    company abc
    Name: 0, dtype: object, 'bp_data':              0   1   2    3
    0  company abc  15  it  2.5}

    [2_20200611111024538569] [INFO] ***********************job 2_20200611111024538569 started***********************
    [2_20200611111024538569] [INFO] progress 0 status INITIATED
    [2_20200611111024538569] [INFO] checking vars
    [2_20200611111024538569] [INFO] progress 10 status RUNNING
    [2_20200611111024538569] [INFO] fetching data from repo
    [2_20200611111024538569] [INFO] extracting data using cli criteria name=company xyz and cli flags skip=10;limit=50 using db mongo_prod
    [2_20200611111024538569] [INFO] saving to .\BusinessPartnersReport_2_20200611111024538569.xlsx
    [2_20200611111024538569] [INFO] saving bp_test_updates sheet
    [2_20200611111024538569] [INFO] saving bp_test_data sheet
    [2_20200611111024538569] [INFO] formatting report .\BusinessPartnersReport_2_20200611111024538569.xlsx
    [2_20200611111024538569] [INFO] complex excel formatting
    [2_20200611111024538569] [INFO] progress 50 status RUNNING
    [2_20200611111024538569] [INFO] upload status to 'https://prod...agement-api/2' -> 50,RUNNING
    [2_20200611111024538569] [INFO] waiting...
    [2_20200611111024538569] [INFO] ok
    [2_20200611111024538569] [INFO] shipping report
    [2_20200611111024538569] [INFO] progress 90 status RUNNING
    [2_20200611111024538569] [INFO] upload status to 'https://prod...agement-api/2' -> 90,RUNNING
    [2_20200611111024538569] [INFO] waiting...
    [2_20200611111024538569] [INFO] ok
    [2_20200611111024538569] [INFO] upload report to 'https://prod...reporting-api' .\BusinessPartnersReport_2_20200611111024538569.xlsx
    [2_20200611111024538569] [INFO] waiting...
    [2_20200611111024538569] [INFO] ok
    [2_20200611111024538569] [INFO] performing cleanup..
    [2_20200611111024538569] [INFO] progress 100 status FINISHED
    [2_20200611111024538569] [INFO] upload status to 'https://prod...agement-api/2' -> 100,FINISHED
    [2_20200611111024538569] [INFO] waiting...
    [2_20200611111024538569] [INFO] ok
    [2_20200611111024538569] [INFO] ************************job 2_20200611111024538569 ended************************
    report {'bp_test_updates': 0    company xyz
    Name: 0, dtype: object, 'bp_test_data':              0     1        2       3
    0  company xyz  2500  finance  366000}