# -----------------------
# common.py
# -----------------------

#from IPython.display import HTML, display
import os, sys
import reprlib
import pandas as pd
import time
from datetime import datetime
from collections import UserDict
from typing import List, Tuple, Iterable, Optional, Union
from concurrent import futures

class Logger:
    def __init__(self, job):
        self.logs = []
        self.job=job

    def info(self, msg):
        self.accumulate('info', msg)
    def warn(self, msg):
        self.accumulate('warn', msg)
    def error(self, msg):
        self.accumulate('error', msg)
    def accumulate(self, flag, message):
        message=message if len(message) < 150 else reprlib.repr(message)
        l = '\n[{gid}] [{flag}] {message}'.format(
          gid=self.job.gid,flag=flag.upper(), message=message)
        self.logs.append(l)
    def __str__(self):
        s=str()
        for l in self.logs:
            s+=str(l)
        return s
        
        

class T(UserDict):
    """flexible tree structure"""
    def __init__(self, **kw):
        super().__init__(self, **kw)
        for k, v in kw.items():
            setattr(self, k, v)

# mock database
DATASTORE={
    'mongo_prod': T(storage='bp_1', events='clients_1'
            , records=[
                ('company xyz', 2500, 'finance', 366000)
                , ('company abc', 15, 'it', 2.5)
                , ('company zzz', 5, 'hr', 5)
            ])
    
}

# run modes
SINGLE, BATCH = 'single', 'batch'

# api urls
JOB_URL = 'https://production.cdq-cloud-engine.com/job-management-api'
REPORT_URL = 'https://production.cdq-cloud-engine.com/reporting-api'

# reporti config variables
# their ids are the same as job ids
RPT_CONFIG = {
    '1': T(

     title='BusinessPartnersUpdatesReport'             
    , report='bp'
    , db='mongo_prod'
    , handler='BPReport'
    , formatter='BPFormatter'
    , output='.'
    ),

    '2': T(

     title='BusinessPartnersReport'             
    , report='bp_test'
    , db='mongo_prod'
    , handler='BPReport'
    , formatter='BPFormatter'
    , output='.'
    )
}
    
class DefaultDbConfig:
    user='user'
    pwd='pwd'
    host='default host'
    
class MongoConfigProd(DefaultDbConfig):
    host='mongo_prod'
    connstring = 'mongo+srv//mongo_prod'


class AppParams:
  def __init__(self, **kw):
    for k, v in kw.items():
        key, val=str(k).lower(), str(v).lower()
        setattr(self, key, val)


class AppParamsHandler:
    """"cli params wrapper"""
    def __init__(self,mode,id,report,storage,event,critera,flags):
        "would handle non cli based params"
        pass
    
    @classmethod
    def fromcli(cls, **kw) -> AppParams:
    
        return AppParams(**kw)
        
# -----------------------
# gateways.py
#   import common
# -----------------------
class APIResponse:
  pass

class ResourceCreated(APIResponse):
  status=201

def handle_response(ctx, request:str, url:str, payload:str=None) -> APIResponse:
  ctx.job.logger.info('waiting...')
  time.sleep(.1)
  ctx.job.logger.info('ok')
  return ResourceCreated()

class APIGateway:
    """handles api upload and update requests-response cycle"""
    def __init__(self, job):
        self.job=job
    def upload(self) -> APIResponse:
        url=reprlib.repr(REPORT_URL)
        file=self.job.report.path
        self.job.logger.info('upload report to {} {}'.format(url, file))
        return handle_response(self, 'put', url) # could be passed as a callback function
        
    def update(self) -> APIResponse:
        progress, status=self.job.progress, self.job.status
        
        url=reprlib.repr('{}/{}'.format(JOB_URL, self.job.jobid))
        self.job.logger.info(
            'upload status to {} -> {},{}'.format(
              url,progress, status))
        return handle_response(self, 'put', url) # could be passed as a callback function

# mock database engine
class Engine:
    """handles db connection and query execution"""
    def __init__(self, connstring:str):
        self.connstring=connstring
        
        #for simplicity assumes host is at the end of connection string
        self.host=self.connstring.split('//')[1]
        
        #mimicks live database instance
        self.instance=DATASTORE[self.host]
        
    def query(self,querystring:str) -> Iterable:
        
        # simple example just to make it work
        # extract first key value pair from querystring
        parsed_query = querystring.split(';')[0].split('=')
        
        #return dummy result
        return filter(lambda x:x[0] == parsed_query[1], self.instance.records)
        
    
# mock mongo client
class MongoClient:
    """instantiates db engine"""
    instance=None 
    @classmethod
    def create_engine(cls, connstring:str) -> Engine:
        if cls.instance is None:
            cls.instance=Engine(connstring)
        
        return cls.instance       

class Engines:
    """predefined db engines"""
    mongo_prod=MongoClient.create_engine(MongoConfigProd.connstring)

    
class Repository:
    pass


    
# ------------------------
# reporting.py
#   import gateways
# ------------------------

class BPRepo(Repository):
    """
        interacts with db engine tied to given report 
        queries database using specified criteria 
        both the db engine as well as the criteria are defined
        within the rpt config class
    """
    def __init__(self, report):
        self.report=report
        self.db = getattr(Engines, report.config.db)
        
    def extract_clients(self) -> pd.DataFrame:
        job=self.report.job
        criteria, flags=job.params.criteria, job.params.flags
        self.report.job.logger.info(
            'extracting data using cli criteria {} and cli flags {} \nusing db {}'.format(
                criteria , flags, self.db.host))
        querystring='{};{}'.format(criteria,flags)
        data = self.db.query(querystring)
        
        return pd.DataFrame(data=data)
    

class ExcelFormatter:pass

class BPFormatter(ExcelFormatter):
    """applies formatting on the existing excel file"""
    def __init__(self, report):
        self.report=report
        
    def __call__(self):
        self.apply()
          
    def apply(self):
        self.report.job.logger.info('complex excel formatting')
        
        
class Report:
    def get_data(self):pass
    def save(self):pass
    def format_report(sefl):pass

class BPReport(Report):
    """fetches data from db, saves to file, applies formatting"""
    def __init__(self, params, job):
        self.params=params
        self.config=RPT_CONFIG[job.jobid]
        self.title=self.config.title
        self.repo=BPRepo(self)
        self.data= {}
        self.path=None
        self.job=job
        
        self.get_data() \
            .save().format_report()
    
    def done(self):
        return True if len(self.data.keys()) > 0 and os.path.exists(self.path) else False
    
    def get_data(self):
        self.job.logger.info('fetching data from repo')
        bp_updates=self.repo.extract_clients()
        report_name=self.config.report
        item = {
            report_name+'_updates': bp_updates.iloc[:, 0]
            ,report_name+'_data': bp_updates
        }
        self.data.update(item)
        return self
        
    def save(self):
        filename='{}_{}.xlsx'.format(self.title, self.job.gid)
        path=os.path.join(self.config.output, filename)
        self.path=path
        self.job.logger.info('saving to {}'.format(path))
        with pd.ExcelWriter(path) as writer:
            for title, data in self.data.items():
                self.job.logger.info('saving {} sheet'.format(title))
                data.to_excel(writer, sheet_name=title)
      
        return self
        
    def format_report(self):
        if self.done():
            self.job.logger.info('formatting report {}'.format(self.path))
            globals()[self.config.formatter](self)()
        else:
            self.job.logger.warn('skipping formatting')
        
        return self
            

        
class ReportFactory:
    """creates the report based on cli report param
    picks the predefined handler using the rpt config class
    """
    def create_report(self,params, job) -> Report:
        config=RPT_CONFIG[params.id]
        if 'bp' in config.report:
            handler=config.handler
            return globals()[handler](params, job)


# ------------------------
# job.py  
#   import reporting
# ------------------------
        
class Job:
    """creates and ships the reports using facory and api gateway
    tracks the execution status
    """
    def __init__(self,iden,params:AppParams):
        
        self.params=params
        self.jobid=iden
        self.gid = '{}_{}'.format(self.jobid, datetime.now().strftime('%Y%m%d%H%M%S%f'))
        self.logger=Logger(self)
        self.logger.info('job {} started'.format(self.gid).center(80, '*'))
        self.progress=0
        self.advance(0)
        
        self.report=None
        self.factory=ReportFactory()
        
        # old job management and reporting client 
        self.apigateway = APIGateway(self)
        
        self.validate()
    
    
    def do_work(self):
        self.create_report()
        self.ship_report()
        self.end()

        return self
    
    def validate(self):
        self.logger.info('checking vars')
        
        self.advance(10)
    
    def advance(self, pct) -> None:
        """updates progress and logs the status"""
        assert pct <=100, 'invalid pct'
        self.progress=pct
        self.show_status()
        
    def create_report(self):
        """call factory to create a report"""
        try:
            self.report=self.factory.create_report(self.params, self)
            self.advance(50)
            self.apigateway.update()
        except Exception as ex:
            self.error(str(ex))
        
        return self
    
    def ship_report(self):
        """sends previously saved excel file to the server"""
        self.logger.info('shipping report')
        self.advance(90)
        self.apigateway.update()
        self.apigateway.upload()
        return self
    
    def end(self):
        """cleanup"""
        self.logger.info('performing cleanup..')
        self.advance(100)
        self.apigateway.update()
        self.logger.info('job {} ended'.format(self.gid).center(80, '*'))
        return self
    
    def show_status(self) -> None:
        msg = "progress {} status {}".format(self.progress, self.status)
        self.logger.info(msg)
    
    @property
    def status(self) -> str:
        INI, WIP, DONE, ERR = 'INITIATED', 'RUNNING', 'FINISHED', 'FAILED'
        status= INI if self.progress==0 else WIP if self.progress < 100 else DONE
        status=ERR if self.progress < 0 else status
        return status
    
    def __call__(self):
        print('calling job')
        self.create_report()
        self.ship_report()
        self.end()
    
    def error(self, msg):
        self.progress=-1
        self.logger.error(msg)
        raise Exception(msg)

class RunResult:
    def __init__(self, job):
        self.job=job

class JobRunner:
    """handles job triggering"""

    def __init__(self, params: AppParams):
      self.params=params
      self.runid='run'+datetime.now().strftime('%Y%m%d%H%M%S')
      params.runid =self.runid

    def run_single(self, id) -> RunResult:
      job_params=self.params

      if self.params.mode==BATCH:
        
        # reinitialize cli params using given id
        # TODO: make it more readable, try to avoid eval and id offsetting
        # TODO: AppParams should handle this
        job_params = AppParamsHandler.fromcli(**eval(self.params.jobs)[int(id)-1])

      job=Job(id, job_params).do_work()

      return RunResult(job)

    def __call__(self) -> Union[RunResult, Iterable[RunResult]]:
      
      # if single mode param id is already defined
      if self.params.mode==SINGLE:
        res = self.run_single(self.params.id)

      # need to get all job ids from parsed args before calling run single
      elif self.params.mode==BATCH:

        # extract job ids from batch params to define num of threads
        # pass each job id to run_single method
        # TODO: avoid eval
        # TODO: AppParams should handle this
        jobs = [v for j in eval(self.params.jobs) for k, v in j.items() if k =='id']
        workers=len(jobs)
        with futures.ThreadPoolExecutor(workers) as executor:
          res = executor.map(self.run_single, jobs)
      
      return res

# ------------------------
# cli.py
# ------------------------

# mock parsed cli args
def parse_cli_args(argv) -> dict:
    
    mode = '-s' if argv[0] is None else argv[0]
    assert mode in ['-s', '-b'], KeyError('invalid mode argument')

    print(mode)
    single= {
           'mode':'single'
           ,'id':'1'  #  will be used to determine the type of report to create
           , 'report':'bp_abc'
           , 'storage':'bp_1'
           , 'events':'clients_1'
           , 'criteria':'name=company abc' 
           , 'flags':'skip=1;limit=10'
           , 'user':os.getenv('username')
          
          }

    # multiple reports
    batch = {

          'mode':'batch'
          ,'jobs':[
            {'id':'1'  
              , 'report':'bp_abc'
              , 'storage':'bp_1'
              , 'events':'clients_1'
              , 'criteria':'name=company abc' 
              , 'flags':'skip=1;limit=10'
            },

            {'id':'2'  
              , 'report':'bp_xyz'
              , 'storage':'bp_1'
              , 'events':'clients_1'
              , 'criteria':'name=company xyz' 
              , 'flags':'skip=10;limit=50'
            }
          ]
           , 'user':os.getenv('username')

      }
    
    mode_map = {'-s':single, '-b':batch}

    return mode_map[mode]
    
  
# ------------------------
# app.py  
#   import cli
#   import job
# ------------------------

def main(argv):
    """
    parses cli params and encapsulates them within AppParam class
    params are passed to the job runner

    params could define a single job run or a batch job
        
    job runner deterimnes the run mode 
    and triggers a job that orchestrates all the work
    
    job is responsible for creating, saving and shipping of the report 
    as well as communicating with job management and reporting apis
    
    job delegates report creation to a factory class
    
    cli params are used to come up with the concrete report implementation
    from within the factory
    
    concrete report implementation uses repository and formatter classes
    to query the database and create final result
    
    database as well as the filter criteria are passed along to the job runner 
    through appParams
     
    """
    app_params = AppParamsHandler.fromcli(**parse_cli_args(argv))
    res = JobRunner(app_params)()
    
    try: # single result
      print(res.job.logger)
      print('report',res.job.report.data)
    except: # batch result
      for r in res:
        print(r.job.logger)
        print('report', r.job.report.data)
    
    return 

if __name__ == "__main__": 
    main(sys.argv[1:])
